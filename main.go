package main

import (
	"fmt"
	"go_ex/go-aws/helper"
	"strings"

	"github.com/tidwall/gjson"
)

type Response struct {
	Message string `json:"message"`
	Util    string `json:"Util"`
}

var jsonPosition = `{
	position : "Project Manager", salary : 100000, subordinates: 
	[
		position : "System Analysis", salary : 50000, subordinates: 
		[
			position : "Programmer", salary : 20000, subordinates:[],
			position : "Tester", salary : 15000, subordinates:[]
		],
		position : "Business Analysis", salary : 50000, subordinates:[]
	]
}
`

func main() {
	trimData(&Response{Message: "  Test Text  "})

	lastOfIndex(&Response{Message: "1234512345", Util: "45"})

	dataUnion()

	countSubordinate(&Response{Message: "Project Manager"})

	findPositionName(&Response{Message: "Position"})

}

func trimData(d *Response) {
	fmt.Println(strings.Trim(d.Message, " "))
}
func lastOfIndex(d *Response) {

	i := strings.LastIndex(d.Message, d.Util)
	fmt.Println(i)
}

func dataUnion() {
	var a = []int{1, 2, 3, 4, 5}
	var b = []int{2, 3, 5, 7, 11}
	fmt.Println(helper.Union(a, b))
}

func countSubordinate(d *Response) {

	result := gjson.Get(jsonPosition, "position."+d.Message+".subordinates")

	for _, subordinate := range result.Array() {
		fmt.Println(subordinate)
		for _, subordinates := range result.Array() {
			fmt.Println(subordinates)
		}
	}
}

func findPositionName(d *Response) {
	result := gjson.Get(jsonPosition, d.Message).String()
	fmt.Println(result)
}
